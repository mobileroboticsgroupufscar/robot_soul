# README #

Para contribuir com o código basta seguir os passos abaixo. 

Para contribuir com a documentação, basta ir a página da Wiki. 

Para maiores informações sobre o projeto basta clicar [aqui](https://bitbucket.org/mobileroboticsgroupufscar/robot_soul/wiki/Home), que você será redirecionado para a Wiki- Home

### Para que serve o repositório? ###

* Versionador do Firmware dos carrinhos (Jason, 
* Documentação do referido Firmware

### Como eu preparo? ###

* Primeiramente, verifique se possui o git instalado. Para usuários Windows, o git não é nativo, portanto faça o download se ainda não o possui. 
Há um tutorial na página de [robótica da UFSCar](http://robotica.ufscar.br/dokuwiki/doku.php?id=reddragons:red&#software_tutorials)

* Se ainda não possui a pasta do projeto, crie uma pasta onde desenvolverá o código, e clone o repositório. Para isso, faça:

```
git clone https://bitbucket.org/mobileroboticsgroupufscar/robot_soul.git
```

Lembre-se que o git clone desse modo pega apenas o MASTER.

Caso deseje contribuir com algum Branch específico, faça:

```
git fetch && git checkout (nome do branch)
```

### Para contribuir ###

* Trabalhe em um branch delineado para a função que deseja mexer, se o branch não existe, crie-o. Quando for a época do release do Firmware, será feita as requisições de ** PULL/MERGE ** em time!
* Não esqueça de dar commit com comentário nas suas modificações
* Caso tenha algum problema para resolver ou uma tarefa a ser feita, adicione no issue tracking
* Sempre que realizar um push, não se esqueça de colocar os comentários na página da WIKI.
